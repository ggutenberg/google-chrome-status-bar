var ggStatusBar = {};

ggStatusBar.heightMultiplier = 0;

ggStatusBar.getBar = function(css) {

	var bar = $('<div id="chrome_extension_statusbar"></div>').on('click', function() {

		clearTimeout($(document).data('timeoutID'));

		bar.delay(500).queue(function() {
			bar.css('display', 'none');
			$(this).dequeue();
		});

	}).on('dblclick', function() {

		clearTimeout($(document).data('timeoutID'));
		$('a').off('mouseover mouseout');
		bar.css('display', 'none');

	});

	for(var key in css) {
		var val = css[key];
		if(val != '') {
			bar.css(key, val);
		}
	}

	ggStatusBar.heightMultiplier = parseInt(bar.css('height'));

	return bar;
};

/*
 Why does the correct width not get returned, either in bar.width() or in some iframes (top banner statcounter)??
*/
ggStatusBar.setHeight = function(bar) {
	//var x = 17; // I don't know why this is required, but it seems to be
	var x = 0;
	var viewportWidth = $(window).width();
	var viewportHeight = $(window).height();
	//var viewportWidth = window.innerWidth;
	//var viewportHeight = window.innerHeight;
	//console.log(viewportWidth);
	//console.log(viewportHeight);
	var rows = 1;
	//console.log(bar.width());
	if((bar.width() + x) > viewportWidth) {
		rows = Math.ceil((bar.width() + x) / viewportWidth);
		//console.log(rows);
	}
	//bar.css('height', (rows * 16) + 'px');
	var height = (rows * ggStatusBar.heightMultiplier) + 'px';
	bar.css('height', (rows * ggStatusBar.heightMultiplier) + 'px');
};

ggStatusBar.showTitle = function(bar) {
	//bar.html(document.title);
	ggStatusBar.setHtml(bar, document.title);
	ggStatusBar.setHeight(bar);
	bar.css('display', 'block');
};

ggStatusBar.setHtml = function(bar, text) {
	bar.html('<div class="left"><a href="http://grinninggecko.com/">Grinning Gecko</a></div><div class="right">' + text + '</div>');
};

ggStatusBar.addMouseEvents = function(bar, titleDelay) {
	var p = parent;
	//bar = p.bar;
	$('a').on('mouseover mouseout', function(event) {
		//console.log($(document));
		// compare document urls with ... what? how do I know which page is the parent?  window.location?

		if (event.type == 'mouseover') {
			// do something on mouseover
			clearTimeout($(document).data('timeoutID'));
			bar.html($(this).attr('href'));
			ggStatusBar.setHeight(bar);
			bar.css('display', 'block');
		} else {
			// do something on mouseout
			clearTimeout($(document).data('timeoutID'));
			$(document).data('timeoutID', setTimeout(function() {
				ggStatusBar.showTitle(bar);
			}, titleDelay));
		}
	});

	/**
	 * Mouseover Opacity
	 */
	//bar.on('mouseenter mouseleave', function(e) {
	//	if ( e.type == 'mouseenter' ) {
	//		$(this).animate({
	//			'opacity': 0.5
	//		}, 500);
	//	}
	//	else {
	//		$(this).animate({
	//			'opacity': 1
	//		}, 500);
	//	}
	//});
};

ggStatusBar.shiftBodyUp = function(bar) {
	height = parseInt(bar.css('height')) + parseInt(bar.css('border-top-width')) + parseInt(bar.css('border-bottom-width'));
	$('body').css('padding-bottom', height + 'px');
	$('body').children().each(function() {
		// Ignore the status bar itself
		if( $(this).attr('id') !== 'chrome_extension_statusbar' ) {
			// position:fixed and bottom:0 (or less than bar.height())
			if( $(this).css('position') === 'fixed' && parseInt($(this).css('bottom')) < parseInt(height) ) {
				$(this).css('bottom', height + 'px');
			}
		}
	});
};

var unTiny = function(data) {
	console.log('untiny');
};

// Called First
$(document).ready(function() {

	//alert('blah');

	chrome.extension.sendMessage({options: "all"}, function(response) {

		var options = response.options;
		var css = response.css;

		if(	(parseInt(options['disable-frames']) == 1 && window == window.top)
			|| parseInt(options['disable-frames']) == 0 ) {

			var bar = ggStatusBar.getBar(css);
			ggStatusBar.addMouseEvents(bar, parseInt(options['title-delay']));
			$('body').append(bar);
			ggStatusBar.showTitle(bar);

		}

		if( window === window.top ) {

			$(window).on('resize', function() {
				ggStatusBar.shiftBodyUp(bar);
			});

			ggStatusBar.shiftBodyUp(bar);
		}

		$('#chrome_extension_statusbar a').on('click', function(e) {
			e.preventDefault();
			var url = 'http://api.longurl.org/v2/expand?url=' + encodeURIComponent('http://t.co/gxH2E0w9') + '&title=1&format=json&callback=?';
			chrome.extension.sendMessage({action: 'getJSON', url: url}, function(data) {
				console.log(data);
			});

			return false;
		});

	});

});

chrome.extension.onMessage.addListener(
	function(request, sender, sendResponse) {
		console.log('message received');
		sendResponse({blah: 'blah'});
	}
)



// Fix iframe support (mouse over link in iframe, bar should only appear in main frame)
// http://my.statcounter.com/project/standard/pageload.php?project_id=2540476 (top ad)

// Need to monitor browser resize event and recalculate
// Is it possible to modify what "bottom" on active page properties refers to?
//   without parsing through every single element on the page, looking for a "bottom" property and adding 20 to it?
//   set window.height = window.height-20 ?
