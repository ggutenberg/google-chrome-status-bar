var ggStatusBar = {};

ggStatusBar.css = {
	'line-height'		:'14px',
	'bottom'			:'0px',
	'right'				:'0px',
	'left'				:'0px',
	'top'				:'',
	'text-align'		:'right',
	'z-index'			:'2147483647',
	'position'			:'fixed',
	'display'			:'none',
	'height'			:'16px',
	'margin'			:'0px',
	'padding'			:'1px 10px 1px 4px',
	'font-size'			:'11px',
	'font-family'		:'"Segoe UI", "Lucida Grande", sans-serif',
	'color'				:'#000',
	'background-color'	:'#eee',
	'border-top'		:'1px solid #aaa',
	'border-left'		:'1px solid #aaa',
	'border-right'		:'1px solid #aaa',
	'border-bottom'		:'none',
	'border-radius'		:'3px 0 0 3px',
	'word-wrap'			:'break-word'
};

ggStatusBar.options = {
	'title-delay'		:5000,
	'disable-frames'	:0
};

ggStatusBar.getCss = function() {
	var css = {};
	for(var key in this.css) {
		//console.log(key);
		css[key] = localStorage.getItem(key);
		//console.log(localStorage.getItem(key));
		if(css[key] == null) {
			css[key] = this.css[key];
		}
	}
	return css;
};

ggStatusBar.getOptions = function() {
	var opt = {};
	for(var key in this.options) {
		opt[key] = localStorage.getItem(key);
		if(opt[key] == null) {
			opt[key] = this.options[key];
		}
	}
	return opt;
}

ggStatusBar.saveOptions = function(pairs) {
	for(var key in pairs) {
		var val = pairs[key];
		localStorage.setItem(key, val);
	}
	return true;
};

ggStatusBar.getDefaults = function() {
	var defaults = {
		'css'		:this.css,
		'options'	:this.options
	};
	return defaults;
};

ggStatusBar.populateOptions = function(options) {
	if(typeof(options) == 'undefined') {
		options = {};
		options['css'] = ggStatusBar.getCss();
		options['options'] = ggStatusBar.getOptions();
	}
	var optHTML = {};
	for(var opt in options) {
		optHTML[opt] = '';
		optHTML[opt] += '<table>';
		for(var key in options[opt]) {
			var val = options[opt][key];
			optHTML[opt] += '<tr><td>';
			switch(key) {
				case 'disable-frames' :
					optHTML[opt] += '<label for="' + key + '">Disable in child frames</label></td><td>';
					var checked = '';
					if(parseInt(val) == 1) {
						checked = 'checked="checked"';
					}
					optHTML[opt] += '<input type="checkbox" id="' + key + '" name="' + key + '" value="' + key + '" ' + checked + '>';
					break;
				case 'title-delay' :
					optHTML[opt] += '<label for="' + key + '">Delay before page Title appears after mousing off a link</label></td><td>';
					optHTML[opt] += '<input type="text" id="' + key + '" name="' + key + '" value=\'' + val + '\'>';
					break;
				default :
					optHTML[opt] += '<label for="' + key + '">' + key + '</label></td><td>';
					optHTML[opt] += '<input type="text" id="' + key + '" name="' + key + '" value=\'' + val + '\'>';
			}
			optHTML[opt] += '</td></tr>';
		}
		optHTML[opt] += '</table>';
		$('#' + opt).html(optHTML[opt]);
	}
};

ggStatusBar.save = function() {
	var pairs = {};
	$('input[type="text"]').each(function() {
		pairs[$(this).attr('id')] = $(this).val();
	});
	$('input[type="checkbox"]').each(function() {
		var val = 0;
		if($(this).is(':checked')) {
			val = 1;
		}
		pairs[$(this).attr('id')] = val;
	});
	if(ggStatusBar.saveOptions(pairs)) {
		alert('Saved!');
	}
};

ggStatusBar.loadDefaults = function() {
	ggStatusBar.populateOptions(ggStatusBar.getDefaults());
};

if(location.protocol === 'chrome-extension:' && location.href.indexOf('options.html') === location.href.length - 'options.html'.length) {
	$(document).ready(function() {
		// On options page
		$('#save').on('click', function() {
			ggStatusBar.save();
		});

		$('#load-defaults').on('click', function() {
			ggStatusBar.loadDefaults();
		});

		ggStatusBar.populateOptions();
	});
}
else {
	// Background
	var log = function(msg) {
		chrome.tabs.getSelected(null, function(tab) {
			chrome.tabs.sendMessage(tabs.id, {action: 'log', msg: msg}, function(response) {
				// nop
			});
		});
	}
	chrome.extension.onMessage.addListener(
		function(request, sender, sendResponse) {
			log('message');
			if (request.options == "all") {
				sendResponse({
					css: ggStatusBar.getCss(),
					options: ggStatusBar.getOptions()
				});
			}
			if(request.action == 'getJSON') {
				$.getJSON(request.url, sendResponse);
			}
		}
	);
}
